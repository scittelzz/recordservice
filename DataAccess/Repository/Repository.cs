﻿using System.Linq;
using DataAccess.AppDbContext;
using DataAccess.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository;
public class Repository<T> : IRepository<T> where T : BaseEntity
{
    private readonly PreRecordContext _dbContext;
    internal DbSet<T> dbSet;
    public Repository(PreRecordContext dbContext)
    {
        _dbContext = dbContext;
        this.dbSet = _dbContext.Set<T>();
    }
    public void Add(T entity)
    {
        dbSet.Add(entity);
        _dbContext.SaveChanges();
    }

    public T Find(int id)
    {
        var item = dbSet.Find(id);
        //return dbSet.Where(x => x.Id == id).FirstOrDefault();
        if (item != null)
        {
            return item;
        }
        else
        {
            throw new InvalidOperationException($"Can't find record with id={id}");
        }
    }

    public IEnumerable<T> GetAll(Guid CustomerId)
    {
        return dbSet.Where(x => x.CustomerId == CustomerId).OrderBy(x => x.Id);
    }

    public void Remove(int id)
    {
        var delItem = dbSet.Where(x => x.Id == id).FirstOrDefault();
        if (delItem != null)
        {
            dbSet.Remove(delItem);
            _dbContext.SaveChanges();
        }
        else
        {
            throw new InvalidOperationException("Can't delete record: Id not found");
        }
    }
}
