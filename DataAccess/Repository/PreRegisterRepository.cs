﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.AppDbContext;
using DataAccess.Interfaces;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Repository;
public class PreRegisterRepository : Repository<CustomerPreRegistration>, IPreRegisterRepository
{
    private readonly PreRecordContext _dbContext;
    public PreRegisterRepository(PreRecordContext dbContext): base(dbContext)
    {
        _dbContext = dbContext;
    }
    public void CreateOrUpdate(CustomerPreRegistration entity)
    {
        var dbEntity = _dbContext.Find<CustomerPreRegistration>(entity.Id);
        if (dbEntity != null)
        {
            dbEntity.Id = entity.Id;
            dbEntity.ServiceId = entity.ServiceId;
            dbEntity.Active = entity.Active;
            dbEntity.PreRegistrationNote = entity.PreRegistrationNote;
            dbEntity.CustomerId = entity.CustomerId;
            dbEntity.DoctorShiftId = entity.DoctorShiftId;
            dbEntity.PreregistrationStart = entity.PreregistrationStart;
            dbEntity.PreregistrationEnd = entity.PreregistrationEnd;
            _dbContext.SaveChanges();
        }
        else 
        {
            _dbContext.Add(entity);
            _dbContext.SaveChanges();
        }
    }
}
