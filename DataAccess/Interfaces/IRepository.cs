﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace DataAccess.Interfaces;

public interface IRepository<T> where T : BaseEntity
{
    /// <summary>
    /// find record in table
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    T Find(int id);
    /// <summary>
    /// find all record for Patient where we know CustomerId of customer
    /// </summary>
    /// <param name="CustomerId"></param>
    /// <returns></returns>
    IEnumerable<T> GetAll(Guid CustomerId);
    /// <summary>
    /// Add record in DB table
    /// </summary>
    /// <param name="entity"></param>
    void Add(T entity);
    /// <summary>
    /// Delete record from DB table where we know id of record
    /// </summary>
    /// <param name="id"></param>
    void Remove (int id);
    /// <summary>
    /// Save data in DB
    /// </summary>
    //void Save();
}
