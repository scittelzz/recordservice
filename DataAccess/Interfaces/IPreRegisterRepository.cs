﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Repository;
using Domain.Entities;

namespace DataAccess.Interfaces;
public interface IPreRegisterRepository : IRepository <CustomerPreRegistration>
{
    void CreateOrUpdate(CustomerPreRegistration entity);
}
