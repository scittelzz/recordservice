﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities;
public class CustomerPreRegistration : BaseEntity
{
    /// <summary>
    /// Id record
    /// </summary>
    [Key]
    public new int Id { get; set; }
    /// <summary>
    /// Id client
    /// </summary>
    public new Guid CustomerId { get; set; }

    /// <summary>
    /// registration start date
    /// </summary>
    public DateTime PreregistrationStart { get; set; }

    public DateTime PreregistrationEnd { get; set; }
    /// <summary>
    /// registration end date
    /// </summary>
    public Guid ServiceId { get; set; }

    /// <summary>
    /// DoctorShift
    /// </summary>
    public Guid DoctorShiftId { get; set; }

    /// <summary>
    /// Note for preregistration
    /// </summary>
    public string PreRegistrationNote { get; set; } = "none";

    /// <summary>
    /// active Yes/No
    /// </summary>
    public bool Active { get; set; }
    public CustomerPreRegistration()
    {
    }
    public CustomerPreRegistration(int id, Guid customerId, DateTime preRegistrationStart, DateTime preRegistrationEnd, Guid serviceId, Guid doctorShift,
                                   string preRegistrationNote, bool active)
    {
        CustomerId = customerId;
        PreregistrationStart = preRegistrationStart;
        PreregistrationEnd = preRegistrationEnd;
        ServiceId = serviceId;
        DoctorShiftId = doctorShift;
        PreRegistrationNote = preRegistrationNote;
        Active = active;
    }
    public string GetRegistration() => $"{CustomerId} registration from {PreregistrationStart} to {PreregistrationEnd}";
}
