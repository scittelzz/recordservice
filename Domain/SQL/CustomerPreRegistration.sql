﻿/****** Object:  Table [dbo].[CustomerPreRegistration]    Script Date: 14.07.2023 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerPreRegistration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [uniqueidentifier] NOT NULL,
	[PreRegistrationStart] [datetime] NULL,
	[PreRegistrationEnd] [datetime] NULL,
	[ServiceId] [uniqueidentifier] NOT NULL,
	[DoctorShiftId] [uniqueidentifier] NULL,
	[PreRegistrationNote] [nvarchar](max) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_CustomerPreRegistration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO