﻿using System.Collections;
using System.Data.SqlTypes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Domain.Entities;
using System.Runtime.InteropServices;
using System;
using DataAccess.Interfaces;
using System.Numerics;

namespace API.Controllers;

[ApiController]
[Route("api/v1/[controller]/[Action]")]
public class PreRegisterController : ControllerBase
{
    private readonly ILogger<PreRegisterController> _logger;
    private readonly IPreRegisterRepository _repository;

    public PreRegisterController(ILogger<PreRegisterController> logger, IPreRegisterRepository repository)
    {
        _logger = logger;
        _repository = repository;
    }
    /// <summary>
    /// select all record for customer
    /// </summary>
    /// <param name="customerId"></param>
    /// <returns></returns>
    [HttpGet]
    public IEnumerable<CustomerPreRegistration> GetAllCustomerRegister(Guid customerId)
    {
        _logger.LogInformation("get info preregistration");
        return _repository.GetAll(customerId);
    }
    /// <summary>
    /// save preregistration entity in DB
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="customerId"> id customer</param>
    /// <param name="preRegistrationStart">visit start date "YYYY-MM-DD HH:MM"</param>
    /// <param name="preRegistrationEnd">visit start date "YYYY-MM-DD HH:MM"</param>
    /// <param name="serviceId">id service</param>
    /// <param name="doctorShiftId">id doctor shift</param>
    /// <param name="note">note for record</param>
    /// <param name="active">true/false acive</param>
   [HttpPost]
    public void AddPreRegistration(Guid customerId, DateTime preRegistrationStart, DateTime preRegistrationEnd, Guid serviceId, Guid doctorShiftId, string note, bool active)
    {
        var block = new CustomerPreRegistration
        {
            CustomerId = customerId,
            PreregistrationStart = preRegistrationStart,
            PreregistrationEnd = preRegistrationEnd,
            ServiceId = serviceId,
            DoctorShiftId = doctorShiftId,
            PreRegistrationNote = note,
            Active = active
        };
        _repository.Add(block);
    }

    /// <summary>
    /// Send phone-number to Patient and return CustomerId
    /// </summary>
    /// <param name="customerPhone"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<Guid> GetCustomerId(string customerPhone)
    {
        return await new HttpClient().GetFromJsonAsync<Guid>($"https://localhost:7001/api/customer/{customerPhone}");
    }
    /// <summary>
    /// request for all serviceid
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<Guid>?> GetServiceId()
    {
        return await new HttpClient().GetFromJsonAsync<List<Guid>>("https://localhost:7001/api/services");
    }
    /// <summary>
    /// request of doctors providing specified service
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<Dictionary<string, Guid>?> GetDoctorsId(Guid serviceId)
    {
        return await new HttpClient().GetFromJsonAsync<Dictionary<string, Guid>>($"https://localhost:7001/api/doctors/{serviceId}");
    }
    /// <summary>
    /// Request for free slots for specified doctors and service
    /// </summary>
    /// <param name="doctorId"></param>
    /// <param name="serviceId"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<List<Guid>?> GetFreeSlots(Guid doctorId, Guid serviceId)
    {
        return await new HttpClient().GetFromJsonAsync<List<Guid>>($"https://localhost:7001/api/slots/{doctorId}&{serviceId}");
    }
    [HttpPost]
    public void BlockSlot(Guid slotId)
    {
        //post slot id for block
    }
}
